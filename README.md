

## App que captura la ubicación(coordenadas de un usuario y guarda con su ubicación, foto del sitio, y muestra esta ubicación en un mapa

Con esta app aprendemos :

1. Persistencia con Core Data
2. Usar cámara y acceder a la galería del teléfono
3. Usar Mapkit y annotations
4. Notificacions(NotificationCenter), mejoras de UI

