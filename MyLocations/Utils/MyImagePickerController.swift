//
//  MyImagePickerController.swift
//  MyLocations
//
//  Created by Lagash Systems on 01/04/2020.
//  Copyright © 2020 Lagash Systems. All rights reserved.
//

import UIKit

class MyImagePickerController: UIImagePickerController{
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}
