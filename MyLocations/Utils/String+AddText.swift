//
//  String+AddText.swift
//  MyLocations
//
//  Created by Lagash Systems on 01/04/2020.
//  Copyright © 2020 Lagash Systems. All rights reserved.
//

import Foundation
extension String{
    mutating func add(text: String?,
                      separatedBy separator: String){
        
        if text != nil {
            if !isEmpty {
                self += separator
            }
        }
        self += text ?? ""
    }
}
